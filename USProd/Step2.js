<script runat="server">
    Platform.Load("Core","1.1.1");

        var DE = "Notify_Me_US_Prod", DEGroupBy = "Notify_Me_US_Prod_GroupBy", prox = new Script.Util.WSProxy(), productIDGroupBy = 0, inventory_Amount = 0, totalCount= 0;
    
        function getInventory(productId)
        {
            var accessToken   = "Bearer ", SFCCAPIDE = "Notify_Me_US_Prod_Token", inventoryAmount = 0;
			var ProdUrl       = 'https://www.carters.com/s/-/dw/data/v20_4/inventory_lists/carters_inventory/product_inventory_records/' + productId; 
       
            var req           = new Script.Util.HttpRequest(ProdUrl);
            req.contentType   = "application/json";
    
            var getRow        = Platform.Function.LookupRows(SFCCAPIDE, 'ID', 1);
            var myAccessToken = accessToken + getRow[0].AccessToken;
    
            req.setHeader("Authorization", myAccessToken);
            req.method = "GET"; 
            try
            {
            var resp        = req.send();
            var resultJSON  = Platform.Function.ParseJSON(String(resp.content));
            inventoryAmount = resultJSON.ats;
            }
            catch(e)
            {
                inventoryAmount = 0;
                return inventoryAmount;
            }
            
            return inventoryAmount;
        }
		
		function getTotalCountFromGroupByTable(productIDGroupBy)
        {
            var resultSetGroupBy = Platform.Function.LookupRows(DEGroupBy,'Product_ID',productIDGroupBy);
            var resultInventory  = resultSetGroupBy[0].Count;
            return resultInventory;    
        }
            
        var colsGroupBy = ["Product_ID"];
        var dataGroupBy = prox.retrieve("DataExtensionObject[" + DEGroupBy + "]", colsGroupBy);

        for (var i = 0; i < dataGroupBy.Results.length; i++)
        {
                var result_listGroupBy = dataGroupBy.Results[i].Properties;
                var obj = {};
                for (k in result_listGroupBy) 
                    {
                            var key = result_listGroupBy[k].Name;
                            var val = result_listGroupBy[k].Value;
                            if (key.indexOf("_") != 0) obj[key] = val;
                            productIDGroupBy = Number(val);
                            inventory_Amount = getInventory(productIDGroupBy); 
							totalCount       = getTotalCountFromGroupByTable(productIDGroupBy);
							         
                            var updateInventoryGroupBy = Platform.Function.UpsertData(DEGroupBy,["Product_ID"],[productIDGroupBy],["Inventory"],[inventory_Amount]); 

                            if ( inventory_Amount > 5)
							{
								if ( inventory_Amount >= totalCount )
									var updateInStockTrue = Platform.Function.UpsertData(DE,["Product_ID"],[productIDGroupBy],["InStock"],[1]);   
								else
									{ 
										var dataRows = Platform.Function.LookupRows(DE,'Product_ID',productIDGroupBy);
										if(dataRows && dataRows.length > 0 )					
											for(var c = 0; c < inventory_Amount; c++) 
											{
												var myGUID = dataRows[c]["GUID"];
			                                    var updateFlag = Platform.Function.UpsertData(DE,["GUID"],[myGUID],["InStock"],[1]);  	
											}												
									}			
							}										
                    }
        }  	
    </script>