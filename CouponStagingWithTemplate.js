const AWS = require('aws-sdk');
const SES = new AWS.SES({ region: "us-east-1" });

exports.handler = async event => {
    const { subject, orderSummary } = JSON.parse(event.body);
    var promotionId   = orderSummary.promotionId;
    var orderNum      = orderSummary.myOrderNum;
    var subTotal      = orderSummary.subTotal;
    var shippingTotal = orderSummary.shippingTotal;
    var promoDiscount = orderSummary.promoDiscount;
    var tax           = orderSummary.tax;
    var ccType        = orderSummary.ccType;
    var ccNumber      = orderSummary.ccNumber;
    var orderTotal    = orderSummary.orderTotal;
        
    var testEmailAddress = "minh.nguyen@carters.com";
    var qaEmailAddress   = ["darshan.patel@carters.com", "dhruvraj.mishra@carters.com", "#eCommQATeam@carters.com", "g.georgiev@astoundcommerce.com", "hiten.savaliya@carters.com", "karina.anaya@carters.com", "celeste.lackey@carters.com", "katie.green@carters.com"];
    var promoQA = "#ecompromos@carters.com";
    
    const params = {
      "Source": testEmailAddress,
      "Template": "CouponStaging1",
      "Destination": {
        "ToAddresses": qaEmailAddress
      },
    };
    
    var templateData = {};
    templateData.promotionId   = promotionId;
    templateData.orderNum      = orderNum;
    templateData.ccNumber      = ccNumber;
    templateData.ccType        = ccType;
    templateData.subTotal      = "$"+subTotal;
    templateData.shippingTotal = "$"+shippingTotal;
    templateData.promoDiscount = "$"+promoDiscount;
    templateData.tax           = "$"+tax;
    templateData.orderTotal    = "$"+orderTotal;
    
    params.TemplateData      = JSON.stringify(templateData);
    
    try {
        await SES.sendTemplatedEmail(params).promise();
        return {
            statusCode: 200,
            body: 'WARN: Auto applied promotion is ON' + promotionId
        }
    } catch (e) {
        console.error(e);
        return {
            statusCode: 400,
            body: 'Sending failed because ' + e
        }
    }
};