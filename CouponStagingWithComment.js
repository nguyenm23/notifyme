// add policy, apply policy to roles
// pass promotionId and orderNum as parameter to api
// verify no auto applied promotion or coupon
// Post call https://vigyqgrrkl.execute-api.us-east-1.amazonaws.com/default/CouponStaging
// PayLoad {
//    "promotionId": "AUTOMATION3",
//    "orderNum": "CARDEV23444",
//    "subject": "test test"
//}
const AWS = require('aws-sdk');
const SES = new AWS.SES({ region: "us-east-1" });

exports.handler = async event => {
    const { subject, orderSummary } = JSON.parse(event.body);
    var promotionId   = orderSummary.promotionId;
    var orderNum      = orderSummary.myOrderNum;
    var subTotal      = orderSummary.subTotal;
    var shippingTotal = orderSummary.shippingTotal;
    var promoDiscount = orderSummary.promoDiscount;
    var tax           = orderSummary.tax;
    var ccType        = orderSummary.ccType;
    var ccNumber      = orderSummary.ccNumber;
    var orderTotal    = orderSummary.orderTotal;
        
    var testEmailAddress = "minh.nguyen@carters.com";
    var qaEmailAddress   = ["darshan.patel@carters.com", "dhruvraj.mishra@carters.com", "eCommQA@carters.com", "g.georgiev@astoundcommerce.com", "hiten.savaliya@carters.com", "karina.anaya@carters.com", "celeste.lackey@carters.com", "katie.green@carters.com"];
    
    const params = {
        Destination: {
            ToAddresses: [testEmailAddress],
        },
        Message: {
            Body: {
              Html: {
               Charset: "UTF-8",
               Data: "<html lang=\"en\">\r\n<head>\r\n    <meta charset=\"UTF-8\">\r\n    <title>Order Summary<\/title>\r\n<\/head>\r\n<body>\r\n    <p> Promotion ID is: <b style='color:red;'> "+ promotionId + " <\/b>. This promotionId has been auto applied to Cart \/ Order Number <b style='color:red;'>"+ orderNum+ "<\/b> in Staging Environment. <br> Please correct the configuration for this promotion \/ coupon to resolve this issue.<\/p>\r\n    \r\n<div style=\"\r\n    box-shadow: whitesmoke 2px 2px 4px 4px;\r\n    font-family: Poppins,Helvetica Neue Light,Helvetica Neue,Helvetica,Arial,sans-serif;\r\n    font-weight: 400;    border-collapse: collapse;\r\n    line-height: 1.3; border-radius: 6px; padding-left: 8px;\r\n    color: #444; padding-top: 1px;\r\n    clear: both;    text-align: left;    box-sizing: border-box;\r\n    font-size: 1rem; margin-left: 412px; margin-top: 100px;\r\nheight: 230px; width: 460px; border-spacing: 2px; text-indent: initial\">\r\n<h2 style=\"font-size: 1.28571rem;padding: 4px\">Order summary<\/h2>\r\n    <table>\r\n        <tr>\r\n            <td style=\"padding: 4px\">Subtotal:<\/td>\r\n            <td style=\"padding-left: 120px\">" + "$"+subTotal + "<\/td>\r\n        <\/tr>\r\n        <tr>\r\n            <td style=\"padding: 4px\">Shipping:<\/td>\r\n            <td style=\"padding-left: 120px\">" + "$"+shippingTotal +"<\/td>\r\n        <\/tr\r\n        <tr>\r\n            <td style=\"padding: 4px\">Promo discount:<\/td>\r\n            <td style=\"padding-left: 120px\">" + "$"+ promoDiscount + "<\/td>\r\n        <\/tr>\r\n        <tr>\r\n            <td style=\"padding: 4px\">Tax:<\/td>\r\n            <td style=\"padding-left: 120px\">" + "$"+ tax + "<\/td>\r\n        <\/tr>\r\n        <tr>\r\n            <td style=\"padding: 4px\">Order charged:<\/td>\r\n        <\/tr>\r\n        <tr>\r\n            <td style=\"font-size: 0.8rem; padding: 4px\"><b>" + ccType + "***" + ccNumber+ "<\/b><\/td>\r\n            <td style=\"padding-left: 120px\"><b>" + "$"+ orderTotal + "<\/b><\/td>\r\n        <\/tr>\r\n    <\/table>\r\n<\/div>\r\n\r\n<\/body>\r\n<\/html>"
               } },
            Subject: {
                Data: subject
            },
        },
        Source: testEmailAddress
    };

    try {
        await SES.sendEmail(params).promise();
        return {
            statusCode: 200,
            body: 'WARN: Auto applied promotion is ON : ' + promotionId
        }
    } catch (e) {
        console.error(e);
        return {
            statusCode: 400,
            body: 'Sending failed because ' + e
        }
    }
};